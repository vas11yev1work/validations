import { useValidation, PropsValidation, Checks } from './useValidation';

export const useMinLength = (
  v: string,
  n: number
): [PropsValidation, Checks, () => void] => {
  const [...values] = useValidation(v, str => str.length >= n);
  return [...values];
};
