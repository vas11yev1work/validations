import { useValidation, PropsValidation, Checks } from './useValidation';

export const useName = (v: string): [PropsValidation, Checks, () => void] => {
  const [...values] = useValidation(v, /^[а-яёА-ЯЁ -]{2,}$/);
  return [...values];
};
