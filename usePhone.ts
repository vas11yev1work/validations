import { useMemo, useState } from 'react';
import { AsYouType, parsePhoneNumber } from 'libphonenumber-js/mobile';

export const usePhone = (
  value: string
): [string, (s: string) => void, boolean, string] => {
  const [phone, setPhone] = useState<string>(value);

  const formattedPhone = useMemo(
    () => (/(.?\d){4,}/.test(phone) ? new AsYouType('RU').input(phone) : phone),
    [phone]
  );

  const isPhoneValid = () => {
    if (phone.trim()) {
      try {
        return parsePhoneNumber(phone, 'RU').isValid();
      } catch {
        return false;
      }
    }
    return false;
  };

  const internationalFormat = () => {
    if (isPhoneValid()) {
      return parsePhoneNumber(phone, 'RU').formatInternational();
    }
    return phone;
  };

  return [formattedPhone, setPhone, isPhoneValid(), internationalFormat()];
};
