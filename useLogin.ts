import { useValidation, PropsValidation, Checks } from './useValidation';

export const useLogin = (v: string): [PropsValidation, Checks, () => void] => {
  const [...values] = useValidation(v, /^[a-z0-9_-]{3,18}$/);
  return [...values];
};
