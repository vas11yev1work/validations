import React from 'react';
import { useMemo, useState } from 'react';

export interface PropsValidation {
  value: string;
  onChange: (e: React.ChangeEvent<HTMLInputElement> | string) => void;
  onBlur: () => void;
  onFocus: () => void;
}

export interface Checks {
  isValid: boolean;
  isTouched: boolean;
  isTyping: boolean;
  isError: boolean;
}

export const useValidation = (
  v: string,
  t: ((str: string) => boolean) | RegExp
): [PropsValidation, Checks, () => void] => {
  const [value, setValue] = useState(v);
  const [isTouched, setIsTouched] = useState(false);
  const [isTyping, setIsTyping] = useState(false);

  const isValid = useMemo(() => {
    if (t instanceof RegExp) {
      return t.test(value);
    }
    return t(value);
  }, [value]);

  const isError = useMemo(() => {
    return !isValid && !isTyping && isTouched;
  }, [isValid, isTyping, isTouched]);

  const onChange = (e: React.ChangeEvent<HTMLInputElement> | string) => {
    if (typeof e === 'string') {
      setValue(e);
      return;
    }
    setValue(e.target.value);
  };

  const onFocus = () => {
    setIsTyping(true);
  };

  const onBlur = () => {
    setIsTyping(false);
    if (value && !isTouched) {
      setIsTouched(true);
    }
  };

  const clear = () => {
    setValue('');
    setIsTouched(false);
  };

  const props: PropsValidation = {
    value,
    onChange,
    onBlur,
    onFocus,
  };

  const checks: Checks = {
    isError,
    isTouched,
    isTyping,
    isValid,
  };

  return [props, checks, clear];
};
